#
# Makefile sanename - a strict name validator
#

SRC=$(wildcard src/*.rs)

target/release/sanename: $(SRC)
	cargo build --release

.PHONY: clean test

test:
	cargo test

clean:
	cargo clean
	rm -f target/
	mkdir target


