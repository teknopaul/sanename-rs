# sanename-rs

![S](doc/sanename-rs.png)

[sanename](sanename.org) provides sensible naming rules to avoid escaping and conversion issues.

This is a Rust package to validate input and convert between the naming conventions supported by sanename.

- `sanename` - the default sanename package naming convention 
- `ClassCase` - JavaClassName naming convention
- `camelCase` - camelCase with lowercase first word naming convention
- `CONSTANT_CASE` - naming convention for constants and environment variables
- `snake_case` - aka kernel_case naming conventions

Also provides a method to sanitize input.
